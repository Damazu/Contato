//
//  DetalhesViewController.swift
//  Contato
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

protocol DetalhesViewControllerDelegate {
    func excluirContato(index: Int)
}

class DetalhesViewController: UIViewController {

    @IBOutlet weak var numero: UILabel!
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var endereco: UILabel!
    
    public var contato: Contato?
    public var index: Int?
    public var delegate: DetalhesViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = contato?.nome
        nome.text = contato?.nome
        numero.text = contato?.numero
        email.text = contato?.email
        endereco.text = contato?.endereco
        
    }
    

    @IBAction func deletarContato(_ sender: Any) {
        
        
        delegate?.excluirContato(index: index!)
        
        
        navigationController?.popViewController(animated: true)
    }
}
