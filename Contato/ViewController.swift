//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit
struct Contato {
    let nome: String
    let numero: String
    let email: String
    let endereco: String
}
class ViewController: UIViewController, UITableViewDataSource {
    var listaDeContatos:[Contato] = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        return cell
    }
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        listaDeContatos.append(Contato(nome: "Contato biel", numero: "31 6666-6666", email: "biel666@email.com", endereco: "Rua Cristal, 99"))
        listaDeContatos.append(Contato(nome: "Contato 2", numero: "31 6666-6666", email: "biel666@email.com", endereco: "Rua Cristal, 99"))
        listaDeContatos.append(Contato(nome: "Contato 3", numero: "31 6666-6666", email: "biel666@email.com", endereco: "Rua Cristal,99"))
        }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "abrirDetalhes" {
            let detalhesViewController = segue.destination as! DetalhesViewController
            let index = sender as! Int
            let contato = listaDeContatos[index]
            detalhesViewController.contato = contato
            detalhesViewController.index = index
            detalhesViewController.delegate = self 
        } else if segue.identifier == "criarContato"{
            let NovoContatoViewController = segue.destination as! NovoContatoViewController
            NovoContatoViewController.delegate = self
        }
    }
}

extension ViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "abrirDetalhes", sender: indexPath.row)
    }
}
extension ViewController: NovoContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato) {
        listaDeContatos.append(contato)
        tableView.reloadData()
    }
}
extension ViewController: DetalhesViewControllerDelegate{
    func excluirContato(index: Int) {
        listaDeContatos.remove(at: index)
        tableView.reloadData()
    }
}
